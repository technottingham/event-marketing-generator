# Tech Nottingham & Women in Tech Tweet Generator

TypeScript app to generate tweets from pre-set templates for TN & WiT events

## Run locally

Install dependencies

```
npm i
```

To build the app:
```
npm run build:dev
```

To watch files for changes and rebuild:

```
npm run watch:dev
```

To run the webserver:

```
npm run start:dev
```

To deploy:

```
sam deploy -g
```