const path = require('path');
const AwsSamPlugin = require('aws-sam-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const awsSamPlugin = new AwsSamPlugin({ vscodeDebug: false });
const LAMBDA_NAME = 'GetLambdaFunction';

module.exports = {
  entry: () => awsSamPlugin.entry(),
  output: {
    filename: (/** @type {any} */ chunkData) => awsSamPlugin.filename(chunkData),
    libraryTarget: 'commonjs2',
    path: path.resolve('.')
  },
  resolve: {
      extensions: ['.ts', '.js']
  },
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [{ fsevents: "require('fsevents')" }],
  module: {
      rules: [
        { 
          test: /\.tsx?$/, 
          loader: 'ts-loader' 
        },
    ]
  },
  plugins: [
    awsSamPlugin, 
    new CopyPlugin({
      patterns: [
        { from: './simple-proxy-api.yml', to: '.aws-sam/build/simple-proxy-api.yml' },
        { from: './src/views', to: `.aws-sam/build/${LAMBDA_NAME}/views` }
      ],
    }),
  ],
};