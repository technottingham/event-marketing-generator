import { GeneratedTweet } from "../../src/models/generatedTweetModel";

export const generatedTweetsMock = {
 tweets : [{"date" : "Monday"}, {"date": "Monday"}] 
}

const generatedTweets = [{
    "isTooLong": false,
    "length": 112,
    "publishDate": "Monday, July 25th 2022",
    "tweet": "Two weeks today!!\nJoin us next Monday for Shirley Says Stuff with Shirley! Plus fun, games and prizes.\n#techNott",
    "tweetId": "tweet-0",
    "shortLink": "https://nott.tech/wit-event",
  },
  {
    "isTooLong": false,
    "length": 98,
    "publishDate": "Monday, August 1st 2022",
    "tweet": "One week today!!\nJoin us next Monday for Shirley Says Stuff! Plus fun, games and prizes.\n#techNott",
    "tweetId": "tweet-1",
    "shortLink": "https://nott.tech/wit-event",
  },
  {
    "isTooLong": false,
    "length": 94,
    "publishDate": "Sunday, August 7th 2022",
    "tweet": "TOMORROW!!!\nJoin us for Shirley Says Stuff with Shirley! Plus fun, games and prizes.\n#techNott",
    "tweetId": "tweet-2",
    "shortLink": "https://nott.tech/wit-event",
  },
  {
    "isTooLong": false,
    "length": 110,
    "publishDate": "Monday, August 8th 2022",
    "tweet": "TONIGHT!!!\nWe're welcoming Shirley for Shirley Says Stuff\nSee you there!\n#techNott https://nott.tech/wit-event",
    "tweetId": "tweet-3",
    "shortLink": "https://nott.tech/wit-event",
  }
];

export const generatedTweetObject = {
    tweets: generatedTweets as Array<GeneratedTweet>
}

export const tweetRequestMock = {
    eventType: "TN",
    talkTitle: "My Talk Title",
    speakerName: "Sally Speaker",
    speakerTitle: "Speaker Title",
    speakerTwitterHandle: "sally",
    shortLink: "nott.tech/short-link",
    eventMonth: "April",
};