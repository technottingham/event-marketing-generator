import { v4 } from 'uuid';
import { Request, NextFunction, Response } from 'express';
import {home, tweets, post } from '../../src/controllers/index.controller';
import { generatedTweetObject, generatedTweetsMock } from '../mocks/tweets';
import { tweetRequestMock } from "../mocks/tweets"

let apiRequestId: string;
let awsRequestId: string;
let reqMock: Request;
let resMock: Response;
let nextMock: NextFunction;

const mockConsoleDebug = jest.spyOn(console, 'debug').mockImplementation(() => jest.fn());
const mockConsoleInfo = jest.spyOn(console, 'info').mockImplementation(() => jest.fn());
const mockConsoleWarn = jest.spyOn(console, 'warn').mockImplementation(() => jest.fn());
const mockConsoleError = jest.spyOn(console, 'error').mockImplementation(() => jest.fn());

describe('Test controller', () => {
    beforeEach(() => {
        apiRequestId = v4();
        awsRequestId = v4();
        reqMock = <Request><unknown>{
          apiGateway: { event: { requestContext: { requestId: apiRequestId } } }
        };
        resMock = <Response><unknown>{ redirect: jest.fn(), render: jest.fn(), status: jest.fn().mockReturnThis() };
        nextMock = jest.fn();
      });
    
      afterEach(() => {
        jest.clearAllMocks();
    });

    describe('Home page', () => {
        it('Return home page', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          await home(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/home');
        });
    });

    describe('Tweets page', () => {
        it('Return the generated tweets page', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          reqMock.body = {
              "eventType":"wit",
              "talkTitle":"Shirley Says Stuff",
              "speakerName":"Shirley",
              "shortLink":"wit-event",
              "eventMonth":"August"
            };
          await tweets(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/tweets',
            expect.objectContaining({
              tweets: expect.any(Array),
              altText: expect.any(String)
            })
          )
      });
    });

    describe('Tweet request validation', () => {
      it('Should throw an error if the talk title is missing', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          reqMock.body = {
            ...tweetRequestMock,
              "talkTitle":"",
            };
          await tweets(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/home', {"error": "Missing talk title"});
          expect(mockConsoleError).toBeCalledWith(expect.anything(), expect.anything(), "Missing talk title");
      });

      it('Should throw an error if the speaker name is missing', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          reqMock.body = {
            ...tweetRequestMock,
              "speakerName":"",
            };
          await tweets(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/home', {"error": "Missing speaker name"});
          expect(mockConsoleError).toBeCalledWith(expect.anything(), expect.anything(), "Missing speaker name");
      });

      it('Should throw an error if the short link is missing', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          reqMock.body = {
            ...tweetRequestMock,
              "shortLink":"",
            };
          await tweets(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/home', {"error": "Missing short link"});
          expect(mockConsoleError).toBeCalledWith(expect.anything(), expect.anything(), "Missing short link");
      });

      it('Should throw an error if invalid month given', async () => {
          const renderMock = jest.spyOn(resMock, 'render');
          reqMock.body = {
            ...tweetRequestMock,
              "eventMonth":"",
            };
          await tweets(reqMock, resMock, nextMock);
          expect(renderMock).toHaveBeenCalledWith('index/home', {"error": "Invalid Month"});
          expect(mockConsoleError).toBeCalledWith(expect.anything(), expect.anything(), "Invalid Month");
      });
    });
})