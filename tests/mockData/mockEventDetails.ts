import { EventType } from "../../src/eventType";
import EventDetails from "../../src/service/eventDetails";

export const mockTweetDetailsTn = {
    talkTitle: "Ethics in Tech",
    speakerName: "Ada Lovelace",
    talkDate: "2021-09-13T18:30:00.000Z",
    shortLink: "nott.tech/tn-sep-2021",
    hashTag: "#techNott",
    eventType: EventType.TN,
    eventDay: "Monday",
    eventMonth: "September",
    speakerTwitterName: "@ada",
    speakerTitle: 'Queen of Being a Badass',
} as unknown as EventDetails;

export const mockTweetDetailsWiT = {
    talkTitle: "Ethics in Tech",
    speakerName: "Ada Lovelace",
    talkDate: "2021-09-02T18:30:00.000Z",
    shortLink: "nott.tech/wit-sep-2021",
    hashTag: "WiTNotts",
    eventType: EventType.WiT,
    eventDay: "Thursday",
    eventMonth: "September",
    speakerTwitterName: "@ada",
    speakerTitle: 'Queen of Being a Badass',
} as unknown as EventDetails;

