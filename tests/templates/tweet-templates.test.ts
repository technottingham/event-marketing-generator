import { getTweetsFromTemplate } from "../../src/templates/tweet-templates";
import { mockTweetDetailsTn } from "../mockData/mockEventDetails";

const stripLineEndings = ( str: string ) : string => {
    return str.replace( /[\r\n]+/gm, "" );
}


describe('Tweets templates', () => {
    it('Returns the generated twitter templates', async () => {
        const result = getTweetsFromTemplate(mockTweetDetailsTn);
        const expected = {
            publishDate: "Monday, August 30th 2021",
            shortLink: "nott.tech/tn-sep-2021",
            tweet: "Two weeks today!!\nJoin us online next Monday for Ethics in Tech with @ada!\nPlus fun, games and prizes.\n#techNott\n"
        };

        expect(result[0].publishDate).toEqual(expected.publishDate);
        expect(result[0].shortLink).toEqual(expected.shortLink);
        expect(stripLineEndings(result[0].tweet)).toMatch(stripLineEndings(expected.tweet));
    });
});