import EventDetails from "../../src/service/eventDetails";
import { getImageAltText } from "../../src/templates/alt-text-templates";
import { mockTweetDetailsTn, mockTweetDetailsWiT } from "../mockData/mockEventDetails";

describe('Alt Text templates', () => {
    test('Returns the generated alt text template for Tech Nottingham', () => {
        const result = getImageAltText(mockTweetDetailsTn as unknown as EventDetails);
        const expected = "Tech Nottingham presents Ethics in Tech by Ada Lovelace - Queen of Being a Badass. Monday, September 13th. Online only at technottingham.com";

        expect(result).toEqual(expected);
    });

    test('Returns the generated alt text template for Women in Tech', () => {
        const result = getImageAltText(mockTweetDetailsWiT);
        const expected = "Women in Tech presents Ethics in Tech by Ada Lovelace - Queen of Being a Badass. Thursday, September 2nd. Online only at technottingham.com";

        expect(result).toEqual(expected);
    });

    test('Ignores speaker job title if none is given', () => {
        const event = {
            ...mockTweetDetailsWiT,
            speakerTitle: '',
        } as unknown as EventDetails;
        const result = getImageAltText(event);
        const expected = "Women in Tech presents Ethics in Tech by Ada Lovelace. Thursday, September 2nd. Online only at technottingham.com";

        expect(result).toEqual(expected);
    });
});
