import * as moment from 'moment'
import { EventType } from "../../src/eventType";
import * as srv from "../../src/util/date-helper";


describe('Date Helper Tests', () => {
    test('Should return date of second Monday in the given month for TN', async () => {
        jest.spyOn(srv, 'getDate').mockReturnValue(moment("2021-03-05"));
        let result = srv.getEventDate("April", EventType.TN);

        expect(result).toEqual(new Date(2021, 3, 12, 18, 30, 0, 0));
    });

   test('Should increment the year if the given month is in the past', async () => {
       const date = new Date();
       const nextYear = new Date(date.getFullYear() + 1, date.getMonth() - 2, date.getDate());
       let result = srv.getEventDate("January", EventType.TN);

       expect(new Date(result).getFullYear()).toEqual(nextYear.getFullYear());
   });

   test('Should not increment the year if the given month is the current month', async () => {
       const date = new Date();
       const thisMonth = new Intl.DateTimeFormat('en-UK', {month: 'long'}).format(date);
       let result = srv.getEventDate(thisMonth, EventType.TN);

       expect(new Date(result).getFullYear()).toEqual(date.getFullYear());
   });

   test('Should return correct date if first day of the month is a Monday for TN', async () => {
       jest.spyOn(srv, 'getDate').mockReturnValue(moment("2021-03-05"));
       let result = srv.getEventDate("November", EventType.TN);

       expect(result).toEqual(new Date(2021, 10, 8, 18, 30, 0, 0));
   });

   test('Should return date of first Thursday in the given month for WiT', async () => {
       jest.spyOn(srv, 'getDate').mockReturnValue(moment("2021-03-05"));
       let result = srv.getEventDate("April", EventType.WiT);

       expect(result).toEqual(new Date(2021, 3, 1, 18, 30, 0, 0));
   });

   test('Should increment the year if the given month is in the past for WiT', async () => {
       const date = new Date();
       const nextYear = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate());
       let result = srv.getEventDate("January", EventType.WiT);

       expect(new Date(result).getFullYear()).toEqual(nextYear.getFullYear());
   });

   test('Should not increment the year if the given month is the current month for WiT', async () => {
       const date = new Date();
       jest.spyOn(srv, 'getDate').mockReturnValue(moment("2021-09-01"));
       let result = srv.getEventDate("September", EventType.WiT);

       expect(new Date(result).getFullYear()).toEqual(date.getFullYear());
   });
});