import { tweetService } from "../../src/service/tweetGeneratorService";
import { tweetRequestMock } from "../mocks/tweets"

describe('Tweet Generator Service', () => {
    it('Should return a list of tweets', async () => {
        let result = tweetService.getTweets(tweetRequestMock);

        expect(Array.isArray(['result'])).toBe(true);
        expect(result.generatedTweets.length).toBe(5);
    });

})