import express, { Router } from 'express';
import { home, tweets, post, callback } from './controllers/index.controller';

const indexRoute: Router = express.Router();

indexRoute.get('/', home);
indexRoute.post('/tweets', tweets);
indexRoute.post('/post', post);
indexRoute.get('/callback', callback);

// TODO - sort this out in api gateway
indexRoute.post('/dev/tweets', tweets);
indexRoute.post('/dev/post', post);
indexRoute.get('/dev/callback', callback);
export default indexRoute;