import { Request } from 'express';

const logFormat = '{ "apiRequestId": "%s", "message": "%s" }';

const debug = (req: Request, msg: string): void => {
  console.debug(logFormat, req.apiGateway?.event.requestContext.requestId, msg);
};

const info = (req: Request, msg: string): void => {
  console.info(logFormat, req.apiGateway?.event.requestContext.requestId, msg);
};

const warn = (req: Request, msg: string): void => {
  console.warn(logFormat, req.apiGateway?.event.requestContext.requestId, msg);
};

const error = (req: Request, msg: string): void => {
  console.error(logFormat, req.apiGateway?.event.requestContext.requestId, msg);
};

export const logger = {
  logFormat,
  debug,
  info,
  warn,
  error,
};