import { EventType } from "../eventType";
import * as moment from 'moment'

function getSecondMonday(startDate: moment.Moment) {
  var startOfMonth = moment(startDate).utc().startOf('month').startOf(<any> 'isoweek');
  var studyDate = moment(startDate).utc().startOf('month').startOf(<any> 'isoweek').add(2, 'w');

  if (studyDate.month() == startOfMonth.month()) {
    studyDate = studyDate.subtract(1, 'w');
  }
  return studyDate;
}


export const getEventDate = (month: string, eventType: EventType) : Date => {
    const postDay = getDate();
    const eventDay = eventType == EventType.TN ? 1 : 4;
    const currentYear = postDay.year();
    let eventDate = moment().year(currentYear).month(month).date(1);

    /** if month is before this month or not the current month, assume the event is for next year */
    if(eventDate.isBefore(postDay) && !eventDate.isSame(postDay, 'month')){
        eventDate.add(1, 'year');
    }

    /** get the first day of the month */
    eventDate.date(1).hour(18).minute(30).seconds(0).milliseconds(0);

    /* keep adding a day until it's the event day (Monday or Thursday) */
    while(eventDate.day() != eventDay){
        eventDate.add(1, 'days');
    }

    /* add another week to get the second Monday of the month for TN events */
    if(eventType == EventType.TN){
      eventDate.add(7, 'days');
    }

    return new Date(eventDate.toISOString()); 
}

export const getDate = () => {
    return moment();
};