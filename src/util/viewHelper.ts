import nunjucks, { Environment } from 'nunjucks';
import { Express } from 'express';
import { format, utcToZonedTime } from 'date-fns-tz';

export const setUpNunjucks = (app: Express): Environment => {
  const env = nunjucks.configure(['views'], {
    autoescape: true,
    express: app,
  }).addGlobal('NODE_ENV', process.env.NODE_ENV)
  // ... any other globals or custom filters here

  return env;
};