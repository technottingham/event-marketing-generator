import { EventType } from "../eventType";

const WIT_SPONSORS : string[] = ["@CapitalOneUK", "@CommifyGroup", "@LSEGplc", "@mhr_solutions"];
const TN_SPONSORS = ["@CapitalOneUK", "@mhr_solutions", "@cronofy"];


export const getSponsorsTwitterHandles = (eventType: EventType) : string => {
    return eventType == EventType.WiT ?
    `${WIT_SPONSORS.slice(0, -1).join(', ')} and ${WIT_SPONSORS.slice(-1)}` :
    `${TN_SPONSORS.slice(0, -1).join(', ')} and ${TN_SPONSORS.slice(-1)}`;
}