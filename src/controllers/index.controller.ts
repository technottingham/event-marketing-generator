import { NextFunction, Request, Response } from 'express';
import { logger } from '../util/logger';
import { tweetService } from '../service/tweetGeneratorService';
import { TweetRequestModel } from '../models/tweetRequestModel';

export const home = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  logger.info(req, 'Home');
  try {
    return res.render('index/home');
  } catch (error) {
    logger.error(req, error);
    return next(error);
  }
}

export const tweets = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  logger.info(req, 'Tweets');
  const body : TweetRequestModel = req.body;
  try {
    tweetService.validateTweetRequestData(body);
  } catch (error) {
    logger.error(req, error.message ?? error);
    return res.render('index/home', {error : error.message ?? error});
  }
  try {
    const generatedTweets = tweetService.getTweets(body);
    return res.render('index/tweets', { tweets : generatedTweets.generatedTweets, altText: generatedTweets.altText });
  } catch (error) {
    logger.error(req, error.message ?? error);
    return next(error);
  }
}

export const post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  logger.info(req, 'Confirm tweets');
  const confirmedTweets = req.body;
  try {
    return res.render(`index/post`, { 'something' : 'confirmed' });
  } catch (error) {
    logger.error(req, error);
    return next(error);
  }
};

export const callback = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  logger.info(req, 'Callback');
  const body = req.body;
  try {
    return res.render(`index/callback`, { 'something' : 'confirmed' });
  } catch (error) {
    logger.error(req, error);
    return next(error);
  }
};
