const Twitter = require('twitter-v2');

const postTweets = (postData: string) : string => {
    return "done";
}

/*
* STEP ONE. Obtain a request token.
* POST oauth/request-token
* oauth_callback="https%3A%2F%2FyourCallbackUrl.com"
* oauth_consumer_key="cChZNFj6T5R0TigYB9yd1w"
*/
const requestToken = async () : Promise<string> => {
    try {
        const client = new Twitter({
          consumer_key: '',
          consumer_secret: '',
          access_token_key: '',
          access_token_secret: '',
        });

        const { data } = await client.get('tweets', { ids: '1228393702244134912' });
        return "token";
    }
    catch(err){
        throw new Error(err);
    }
}

export const tweetPostService = {
    postTweets,
    requestToken
};
