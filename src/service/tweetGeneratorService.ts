import { EventType } from "../eventType";
import EventDetails from "../service/eventDetails";
import { TweetRequestModel } from '../models/tweetRequestModel';
import * as moment from 'moment'
import { GeneratedTweet } from "../models/generatedTweetModel";
import { getTweetsFromTemplate } from "../templates/tweet-templates";
import { logger } from "../util/logger";
import { getImageAltText } from "../templates/alt-text-templates";
import { GeneratedTweets } from "../models/generatedTweetsData";

const getTweets = (tweetRequest: TweetRequestModel) : GeneratedTweets =>
{
    validateTweetRequestData(tweetRequest);
    const eventDetails = new EventDetails(tweetRequest);
    let tweets = getTweetsFromTemplate(eventDetails);
    let generatedTweets : Array<GeneratedTweet> = [];
    let index = 0;
    tweets.forEach(template => {
        const generatedTweet : GeneratedTweet = new GeneratedTweet(index, template.tweet, template.publishDate, template.shortLink);
        generatedTweets.push(generatedTweet);
        index++;
    });

    return {
        generatedTweets : generatedTweets,
        altText: getAltText(eventDetails)
    }
}

const getAltText = (eventDetails: EventDetails) :string => {
    return getImageAltText(eventDetails);
}

const validateTweetRequestData = (tweetRequest: TweetRequestModel) : void => {
    if(!tweetRequest.eventType || typeof tweetRequest.eventType == 'undefined'){
        throw new Error("Missing Type of Event. Choose Women in Tech or Tech Nottingham.");
    }

    if(!tweetRequest.talkTitle || typeof tweetRequest.talkTitle == 'undefined'){
        throw new Error("Missing talk title");
    }

    if(!tweetRequest.speakerName || typeof tweetRequest.speakerName == 'undefined'){
        throw new Error("Missing speaker name");
    }

    if(!tweetRequest.shortLink || typeof tweetRequest.shortLink == 'undefined'){
        throw new Error("Missing short link");
    }

    if(!moment(tweetRequest.eventMonth, 'MMMM').isValid()){
        throw new Error("Invalid Month");
    }
}

export const tweetService = {
    getTweets,
    validateTweetRequestData
};