import { EventType } from "../eventType";
import { TweetRequestModel } from "../models/tweetRequestModel";
import { getEventDate } from "../util/date-helper";

export default class EventDetails {
    talkTitle!: string;
    speakerName!: string;
    talkDate!: Date;
    shortLink!: string;
    hashTag!: string;
    eventType!: EventType;
    eventDay!: string;
    eventMonth!: string;
    speakerTwitterName: string;
    speakerTitle: string;

    constructor(data: TweetRequestModel) {
        this.talkTitle = data.talkTitle;
        this.speakerName = data.speakerName;
        this.shortLink = `https://nott.tech/${data.shortLink}`;
        this.eventType = data.eventType.toUpperCase() === "WIT" ? EventType.WiT : EventType.TN,
        this.speakerTwitterName = data.speakerTwitterHandle ? `@${data.speakerTwitterHandle}` : data.speakerName;
        this.speakerTitle = data.speakerTitle;

        this.eventMonth = data.eventMonth;

        this.talkDate = this.getTalkDate();
        this.hashTag = `#${this.getHashTag()}`;
        this.eventDay = this.getEventDay();
    }
    private getTalkDate(): Date {
        return getEventDate(this.eventMonth, this.eventType);
    }

    private getHashTag(): string {
        return this.eventType == EventType.TN ?
            "techNott" :
            "WiTNotts"
    }

    private getEventDay(): string {
        return this.eventType == EventType.TN ?
            "Monday" :
            "Thursday"
    }
}