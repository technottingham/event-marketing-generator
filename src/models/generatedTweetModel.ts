export class GeneratedTweet{
    public tweet!: string;
    public shortLink!: string;
    public tweetId!: string;
    public length!: number;
    public publishDate!: string;
    public isTooLong!: boolean;

    constructor(tweetNumber: number, content: string, publishDate: string, shortLink: string) {
        this.tweetId = `tweet-${tweetNumber}`;
        this.tweet = content;
        this.length = content.length;
        this.publishDate = publishDate;
        this.isTooLong = content.length > 280;
        this.shortLink = shortLink;
    }
}