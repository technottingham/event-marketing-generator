import { GeneratedTweet } from "./generatedTweetModel";

export interface GeneratedTweets{
    generatedTweets : Array<GeneratedTweet>,
    altText: string
}
