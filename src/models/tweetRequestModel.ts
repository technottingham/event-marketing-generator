export interface TweetRequestModel{
    eventType: string;
    talkTitle: string;
    speakerName: string;
    speakerTwitterHandle: string;
    shortLink: string;
    eventMonth: string;
    speakerTitle: string;
}