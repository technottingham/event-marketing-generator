export interface TweetTemplateModel{
  tweet : string,
  publishDate: string,
  shortLink : string
}
