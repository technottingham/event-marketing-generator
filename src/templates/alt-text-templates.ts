import * as moment from 'moment'
import EventDetails from "../service/eventDetails";

export const getImageAltText = (event: EventDetails) : string => {
    const formattedTalkDate = moment(event.talkDate).format("dddd, MMMM Do");
    const speakerTitle = event.speakerTitle ? ` - ${event.speakerTitle}` : '';
    return `${event.eventType} presents ${event.talkTitle} by ${event.speakerName}${speakerTitle}. ${formattedTalkDate}. Online only at technottingham.com`;
}