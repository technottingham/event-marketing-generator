import { TweetTemplateModel } from "../models/tweetTemplateModel";
import EventDetails from "../service/eventDetails";
import * as moment from 'moment'
import { EventType } from "../eventType";
import { getSponsorsTwitterHandles } from "../util/sponsors-helper";

export const getTweetsFromTemplate = (eventDetails: EventDetails) : Array<TweetTemplateModel> => {
    return [
        {
            tweet : `Two weeks today!!\n\nJoin us online next ${eventDetails.eventDay} for ${eventDetails.talkTitle} with ${eventDetails.speakerTwitterName}!\n\nPlus fun, games and prizes.\n\n${eventDetails.hashTag}\n\n`,
            publishDate: moment(eventDetails.talkDate).subtract(14, 'days').format("dddd, MMMM Do YYYY"),
            shortLink : eventDetails.shortLink
        },
        {
            tweet : getSponsorTweet(eventDetails),
            publishDate: moment(eventDetails.talkDate).subtract(eventDetails.eventType == EventType.WiT ? 2 : 4, 'days').format("dddd, MMMM Do YYYY"),
            shortLink : eventDetails.shortLink
        },
        {
            tweet : `One week today!!\n\nJoin us online next ${eventDetails.eventDay} for ${eventDetails.talkTitle}! Plus fun, games and prizes.\n\n${eventDetails.hashTag}\n\n`,
            publishDate: moment(eventDetails.talkDate).subtract(7, 'days').format("dddd, MMMM Do YYYY"),
            shortLink : eventDetails.shortLink
        },
        {
            tweet : `${getTomorrowDay(eventDetails.eventType)}!!!\n\n${eventDetails.speakerTwitterName} presents "${eventDetails.talkTitle}"!\n\nOnline on Zoom from 18.30.\n\n${eventDetails.hashTag}\n\n`,
            publishDate: moment(eventDetails.talkDate).subtract(eventDetails.eventType == EventType.WiT ? 1 : 3, 'days').format("dddd, MMMM Do YYYY"),
            shortLink : eventDetails.shortLink
        },
        {
            tweet : `TONIGHT!!!\n\nWe're welcoming ${eventDetails.speakerTwitterName} for ${eventDetails.talkTitle}\n\nSee you there!\n\n${eventDetails.hashTag}\n\n`,
            publishDate: moment(eventDetails.talkDate).format("dddd, MMMM Do YYYY"),
            shortLink : eventDetails.shortLink
        },
    ];
}

const getSponsorTweet = (eventDetails: EventDetails) : string => {
    return eventDetails.eventType == EventType.WiT ?
        `Not long to go until ${eventDetails.hashTag}!!\n\nWe couldn't do it without the help and support from our amazing sponsors - ${getSponsorsTwitterHandles(eventDetails.eventType)}.\n\nThank you!!\n\n` :
        `Not long to go until ${eventDetails.hashTag}!!\n\nWe couldn't do it without the help and support from our amazing sponsors - ${getSponsorsTwitterHandles(eventDetails.eventType)}.\n\nThank you!!\n\n`;
}

const getTomorrowDay = (eventType: EventType) : string => {
    return eventType == EventType.WiT ?
       `TOMORROW` :
       `MONDAY`;
}